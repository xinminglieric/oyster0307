/*
 * Copyright 2013 John Talburt, Eric Nelson
 *
 * This file is part of Oyster created in the ERIQ Research Center at University of Arkansas at Little Rock.
 * 
 * Oyster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Oyster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Oyster.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package edu.ualr.oyster.utilities;

import java.util.Iterator;
import java.util.Locale;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * NGramFingerprint is based on the Google Refine Method N-Gram Fingerprint 
 * methodology:
 * The n-gram fingerprint method is similar to the fingerprint method described 
 * above but instead of using whitespace separated tokens, it uses n-grams, 
 * where the n (or the size in chars of the token) can be specified by the user. 
 * 
 * Here is what it does (or check the code if you're curious about the details, 
 * again the order is significant): 
 * <ul>
 * <li>change all characters to their lowercase representation 
 * <li>remove all punctuation, whitespace, and control characters 
 * <li>obtain all the string n-grams 
 * <li>sort the n-grams and remove duplicates 
 * <li>join the sorted n-grams back together 
 * <li>normalize extended western characters to their ASCII representation 
 * <ul>
 * So, for example, the 2-gram fingerprint of "Paris" is "arispari" and the 
 * 1-gram fingerprint is "aiprs". 
 * Why is this useful? In practice, using big values for n-grams doesn't yield 
 * any advantage over the previous fingerprint method, but using 2-grams and 
 * 1-grams, while yielding many false positives, can find clusters that the 
 * previous method didn't find even with strings that have small differences, 
 * with a very small performance price. 
 * 
 * For example "Krzysztof", "Kryzysztof" and "Krzystof" have different lengths 
 * and different regular fingerprints, but share the same 1-gram fingerprint 
 * because they use the same letters. 
 * 
 * Created on Apr 29, 2012 8:18:14 AM
 * @author Eric D. Nelson
 */
public class NGramFingerprint {
    /**
     * Creates a new instance of NGramFingerprint
     */
    public NGramFingerprint(){
    }

    public String getNGramFingerprint(String s, int ngram_size) {
        String result = null, temp = "";
        try {
            if (ngram_size < 0) {
                ngram_size = 2;
            }

            if (s != null) {
                temp = s.toLowerCase(Locale.US);
                temp = temp.replaceAll("\\p{Punct}|\\p{Cntrl}|\\p{Space}", "");
                TreeSet<String> set = ngram_split(temp, ngram_size);
                StringBuilder sb = new StringBuilder();
                Iterator<String> i = set.iterator();

                while (i.hasNext()) {
                    sb.append(i.next());
                }
                result = sb.toString();
            }
        } catch (RuntimeException ex) {
            Logger.getLogger(NGramFingerprint.class.getName()).log(Level.SEVERE, "s:" + s + " temp:" + temp + " result:" + result, ex);
            result = null;
        }
        return result;
    }

    public boolean compareNGramFingerprints(String s, String t, int length){
        String sFingerprint = getNGramFingerprint(s, length);
        String tFingerprint = getNGramFingerprint(t, length);
        
        boolean flag = false;
        if (sFingerprint != null && tFingerprint != null) {
            flag = sFingerprint.equals(tFingerprint);
        }
        
        return flag;
    }

    protected TreeSet<String> ngram_split(String s, int size) {
        TreeSet<String> set = new TreeSet<String>();
        char[] chars = s.toCharArray();
        for (int i = 0; i + size <= chars.length; i++) {
            set.add(new String(chars, i, size));
        }
        return set;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
    }

}
