/*
 * Copyright 2010 John Talburt, Eric Nelson
 *
 * This file is part of Oyster created in the ERIQ Research Center at University of Arkansas at Little Rock.
 * 
 * Oyster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Oyster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Oyster.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package edu.ualr.oyster.utilities;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import edu.ualr.oyster.formatter.ErrorFormatter;

/**
 * This class is used to find the agreement weight for certain value.
 * Each line of the text file specified by the WgtTable must have two, table-separated values.
 * The first value of the attribute defined by the Item attribute of the <Term>. 
 * The second value in each line of the text file is an integer value containing only decimal digits with an optional prefix plus symbol or a minus prefix symbol in the case of negative values, e.g. "100", "+100", or "-100".
 * The integer value represents the agreement weight for the corresponding attribute value given as the first value.
 * <ul>
 * <li>Two column tab delimited</li>
 * <li>Any comments must be preceded by !! </li>
 * </ul>
 * @author Cheng Chen
 */

// <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
// #[regen=yes,id=DCE.C4D7B66D-D500-28C1-7A09-6DF8995F661F]
// </editor-fold> 
public class OysterWeightTable {

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,id=DCE.38B435FA-4A07-AC7B-A35B-741DA17592BB]
    // </editor-fold> 
    /** The weight table */
    private Map <String, Integer> weightTable;


    /**
     * Creates a new instance of <code>OysterNickNameTable</code>
     * @param filename the data file to be loaded
     */
    public OysterWeightTable (String filename) {
	    weightTable = new LinkedHashMap <String, Integer>();
        load(filename);
    }

    /**
     * Loads the data file into the nickname table
     * @param filename the file to be loaded
     */
    private void load(String filename){
        int count = 0;
        String read;
        String [] text;
        BufferedReader infile = null;
        
        try{
            File file = new File(filename);
//            System.out.println("Loading " + file.getName());
            infile = new BufferedReader(new FileReader(file));
            while((read = infile.readLine()) != null){
                if (!read.startsWith("!!")) {
                    text = read.split("[\t]");
                    String value=text[0].toLowerCase(Locale.US);
                    Integer weight=Integer.parseInt(text[1]);
                    weightTable.put(value, weight);
                    count++;
                }
            }
            
//            System.out.println(count + " elements loaded.\n");
        } catch(IOException ex){
            Logger.getLogger(OysterWeightTable.class.getName()).log(Level.WARNING, ErrorFormatter.format(ex), ex);
        } finally {
            try {
                if (infile != null) {
                    infile.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(OysterWeightTable.class.getName()).log(Level.SEVERE, ErrorFormatter.format(ex), ex);
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,regenBody=yes,id=DCE.0C76356C-4678-E026-2018-88464B38F4ED]
    // </editor-fold> 
    /**
     * Returns the Weight Table for this <code>OysterWeightTable</code>
     * @return the Weight Table
     */
    public Map <String, Integer> getWeightTable () {
        return weightTable;
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,regenBody=yes,id=DCE.EF9C84C0-CB11-EA2A-BEF6-6CA8722E56C0]
    // </editor-fold> 
    /**
     * Sets the Weight Table for this <code>OysterWeightTable</code>
     * @param wightTable the nickname table to be set
     */
    public void setWeightTable (Map <String, Integer> weightTable) {
        this.weightTable = weightTable;
    }
}

