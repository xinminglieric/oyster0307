/*
 * Copyright 2010 John Talburt, Eric Nelson
 *
 * This file is part of Oyster created in the ERIQ Research Center at University of Arkansas at Little Rock.
 * 
 * Oyster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Oyster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Oyster.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package edu.ualr.oyster.utilities;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * In its current implementation, BMPM' is primarily concerned with matching 
 * surnames of Ashkenazic Jews. This is due to the list of languages whose 
 * graphic and phonetic features are already taken into account.  These 
 * languages are Russian written in Cyrillic letters, Russian transliterated 
 * into English letters, Polish, German, Romanian, Hungarian, Hebrew written in 
 * Hebrew letters, French, Spanish, and English. The name matching is also 
 * applicable to non-Jewish surnames from the countries in which those languages
 * are spoken.
 * 
 * @see http://stevemorse.org/phonetics/bmpm.htm
 * @see http://stevemorse.org/phonetics/bmpm2.htm
 * Created on Sept 16, 2011
 * @author Eric D. Nelson
 */
public class BeiderMorseSoundex {
    public final static String ANY        = "any";
    public final static String ARABIC     = "arabic";
    public final static String CZECH      = "czech";
    public final static String CYRILLIC   = "cyrillic";
    public final static String DUTCH      = "dutch";
    public final static String ENGLISH    = "english";
    public final static String FRENCH     = "french";
    public final static String GERMAN     = "german";
    public final static String GREEK      = "greek";
    public final static String GREEKLATIN = "greeklatin";
    public final static String HEBREW     = "hebrew";
    public final static String HUNGARIAN  = "hungarian";
    public final static String ITALIAN    = "italian";
    public final static String POLISH     = "polish";
    public final static String PORTUGUESE = "portuguese";
    public final static String ROMANIAN   = "romanian";
    public final static String RUSSIAN    = "russian";
    public final static String SPANISH    = "spanish";
    public final static String TURKISH    = "turkish";
    
    /**
     * Creates a new instance of BeiderMorseSoundex
     */
    public BeiderMorseSoundex(){
    }
    
    public String getSoundex(String s) {
        String result = null, temp = "";

        try {
            // Step 1. Identifying the Language
            // Step 2: Calculating the Exact Phonetic Value
            // Step 3: Calculating the Approximate Phonetic Value
            // Step 4: Calculating the Hebrew Phonetic Value
        } catch (Exception ex) {
            Logger.getLogger(BeiderMorseSoundex.class.getName()).log(Level.SEVERE, "s:" + s + ")) { set.add(temp:" + temp + ")) { set.add(result:" + result, ex);
            result = null;
        }
        return result;
    }
    
    //==========================================================================
    // Step 1. Identifying the Language
    //==========================================================================
    // FIXME: Change the characters to UTF-8 enitiy Ref's (use CovertData2UTF8 class)
    private String identiyLanguage(String s){
        String result = "";
        Set<String> set = new LinkedHashSet<String>();
        
        // 1. following are rules to accept the language
        // 1.1 Special letter combinations
        if (matches(s, "uu")) {
            set.add(DUTCH);
        } else if (matches(s, "sj$")) {
            set.add(DUTCH);
        } else if (matches(s, "^sj")) {
            set.add(DUTCH);
        }
        
        if (matches(s, "^oâ€™")) {
            set.add(ENGLISH);
        } else if (matches(s, "^o'")) {
            set.add(ENGLISH);
        } else if (matches(s, "^mc")) {
            set.add(ENGLISH);
        } else if (matches(s, "^fitz")) {
            set.add(ENGLISH);
        } else if (matches(s, "field$")) {
            set.add(ENGLISH);
        }
        
        if (matches(s, "ault$")) {
            set.add(FRENCH);
        } else if (matches(s, "oult$")) {
            set.add(FRENCH);
        } else if (matches(s, "eux$")) {
            set.add(FRENCH);
        } else if (matches(s, "eix$")) {
            set.add(FRENCH);
        } else if (matches(s, "guy")) {
            set.add(FRENCH);
        }
        
        if (matches(s, "witz")) {
            set.add(GERMAN);
        } else if (matches(s, "mann")) {
            set.add(GERMAN);
        } else if (matches(s, "stein")) {
            set.add(GERMAN);
        } else if (matches(s, "heim$")) {
            set.add(GERMAN);
        } else if (matches(s, "heimer$")) {
            set.add(GERMAN);
        } else if (matches(s, "thal")) {
            set.add(GERMAN);
        } else if (matches(s, "zweig")) {
            set.add(GERMAN);
        } else if (matches(s, "[aeou]h")) {
            set.add(GERMAN);
        } else if (matches(s, "Ã¤h")) {
            set.add(GERMAN);
        } else if (matches(s, "Ã¶h")) {
            set.add(GERMAN);
        } else if (matches(s, "Ã¼h")) {
            set.add(GERMAN);
        } else if (matches(s, "chsch")) {
            set.add(GERMAN);
        } else if (matches(s, "tsch")) {
            set.add(GERMAN);
        }
        
        if (matches(s, "glou$")) {
            set.add(GREEKLATIN);
        } else if (matches(s, "poulos$")) {
            set.add(GREEKLATIN);
        } else if (matches(s, "pulos$")) {
            set.add(GREEKLATIN);
        } else if (matches(s, "iou")) {
            set.add(GREEKLATIN);
        }
        
        if (matches(s, "cs$")) {
            set.add(HUNGARIAN);
        } else if (matches(s, "^cs")) {
            set.add(HUNGARIAN);
        } else if (matches(s, "dzs")) {
            set.add(HUNGARIAN);
        } else if (matches(s, "zs$")) {
            set.add(HUNGARIAN);
        } else if (matches(s, "^zs")) {
            set.add(HUNGARIAN);
        } else if (matches(s, "gy$")) {
            set.add(HUNGARIAN);
        } else if (matches(s, "gy[aeou]")) {
            set.add(HUNGARIAN);
        }
        
        if (matches(s, "etti$")) {
            set.add(ITALIAN);
        } else if (matches(s, "eti$")) {
            set.add(ITALIAN);
        } else if (matches(s, "ati$")) {
            set.add(ITALIAN);
        } else if (matches(s, "ato$")) {
            set.add(ITALIAN);
        } else if (matches(s, "[aoei]no$")) {
            set.add(ITALIAN);
        } else if (matches(s, "[aoei]ni$")) {
            set.add(ITALIAN);
        } else if (matches(s, "esi$")) {
            set.add(ITALIAN);
        } else if (matches(s, "oli$")) {
            set.add(ITALIAN);
        }
        
        if (matches(s, "^rz")) {
            set.add(POLISH);
        } else if (matches(s, "[bcdfgklmnpstwz]rz")) {
            set.add(POLISH);
        } else if (matches(s, "rz[bcdfghklmnpstw]")) {
            set.add(POLISH);
        } else if (matches(s, "cki$")) {
            set.add(POLISH);
        } else if (matches(s, "ska$")) {
            set.add(POLISH);
        } else if (matches(s, "cka$")) {
            set.add(POLISH);
        } else if (matches(s, "cz")) {
            set.add(POLISH);
        } else if (matches(s, "niew")) {
            set.add(POLISH);
        } else if (matches(s, "^wl")) {
            set.add(POLISH);
        }
        
        if (matches(s, "[ln]h[ao]$")) {
            set.add(PORTUGUESE);
        }
        
        if (matches(s, "eau")) {
            set.add(ROMANIAN);
        } else if (matches(s, "escu$")) {
            set.add(ROMANIAN);
        } else if (matches(s, "esco$")) {
            set.add(ROMANIAN);
        } else if (matches(s, "vici$")) {
            set.add(ROMANIAN);
        } else if (matches(s, "schi$")) {
            set.add(ROMANIAN);
        }
        
        if (matches(s, "ii$")) {
            set.add(RUSSIAN);
        } else if (matches(s, "iy$")) {
            set.add(RUSSIAN);
        } else if (matches(s, "yy$")) {
            set.add(RUSSIAN);
        } else if (matches(s, "yi$")) {
            set.add(RUSSIAN);
        }
        
        if (matches(s, "tx")) {
            set.add(SPANISH);
        } else if (matches(s, "gÃ¼e")) {
            set.add(SPANISH);
        } else if (matches(s, "gÃ¼i")) {
            set.add(SPANISH);
        }
        
        if (matches(s, "ceau")) {
            set.add(FRENCH);
            set.add(ROMANIAN);
        }
        if (matches(s, "tz$")) {
            set.add(GERMAN);
            set.add(RUSSIAN);
            set.add(ENGLISH);
        }
        if (matches(s, "^tz")) {
            set.add(RUSSIAN);
            set.add(ENGLISH);
        }
        if (matches(s, "ghe")) {
            set.add(ROMANIAN);
            set.add(GREEKLATIN);
        } else if (matches(s, "ghi")) {
            set.add(ROMANIAN);
            set.add(GREEKLATIN);
        }
        if (matches(s, "rz$")) {
            set.add(POLISH);
            set.add(GERMAN);
        }
        if (matches(s, "ae")) {
            set.add(GERMAN);
            set.add(RUSSIAN);
            set.add(ENGLISH);
        }
        if (matches(s, "oe")) {
            set.add(GERMAN);
            set.add(FRENCH);
            set.add(RUSSIAN);
            set.add(ENGLISH);
            set.add(DUTCH);
        }
        if (matches(s, "th$")) {
            set.add(GERMAN);
            set.add(ENGLISH);
        }
        if (matches(s, "^th")) {
            set.add(GERMAN);
            set.add(ENGLISH);
            set.add(GREEKLATIN);
        }
        if (matches(s, "cy")) {
            set.add(POLISH);
            set.add(GREEKLATIN);
        }
        if (matches(s, "[ln]h[aou]")) {
            set.add(PORTUGUESE);
            set.add(FRENCH);
            set.add(GERMAN);
            set.add(DUTCH);
            set.add(CZECH);
            set.add(SPANISH);
            set.add(TURKISH);
        }
        
        if (matches(s, "sch$")) {
            set.add(GERMAN);
            set.add(RUSSIAN);
        } else if (matches(s, "^sch")) {
            set.add(GERMAN); 
            set.add(RUSSIAN);
        }
        
        if (matches(s, "ck$")) {
            set.add(GERMAN);
            set.add(ENGLISH);
        }
        if (matches(s, "c$")) {
            set.add(POLISH);
            set.add(ROMANIAN);
            set.add(HUNGARIAN);
            set.add(CZECH);
            set.add(TURKISH);
        }
        if (matches(s, "sz")) {
            set.add(POLISH);
            set.add(HUNGARIAN);
        }
        if (matches(s, "^wr")) {
            set.add(POLISH);
            set.add(ENGLISH);
            set.add(GERMAN);
            set.add(DUTCH);
        }
        if (matches(s, "gy")) {
            set.add(HUNGARIAN);
            set.add(RUSSIAN);
            set.add(FRENCH);
            set.add(GREEKLATIN);
        }
        if (matches(s, "gu[ei]")) {
            set.add(SPANISH);
            set.add(FRENCH);
            set.add(PORTUGUESE);
        }
        if (matches(s, "gu[ao]")) {
            set.add(SPANISH);
            set.add(PORTUGUESE);
        }
        if (matches(s, "gi[aou]")) {
            set.add(ITALIAN);
            set.add(GREEKLATIN);
        }

        if (matches(s, "ly")) {
            set.add(HUNGARIAN);
            set.add(RUSSIAN);
            set.add(POLISH);
            set.add(GREEKLATIN);
        } else if (matches(s, "ty")) {
            set.add(HUNGARIAN);
            set.add(RUSSIAN);
            set.add(POLISH);
            set.add(GREEKLATIN);
        }
        
        if (matches(s, "ny")) {
            set.add(HUNGARIAN);
            set.add(RUSSIAN);
            set.add(POLISH);
            set.add(SPANISH);
            set.add(GREEKLATIN);
        }
        

        // 1.2 special characters  
        if (matches(s, "Ä�")) {
            set.add(CZECH);
        } else if (matches(s, "Ä�")) {
            set.add(CZECH);
        } else if (matches(s, "Åˆ")) {
            set.add(CZECH);
        } else if (matches(s, "Å™")) {
            set.add(CZECH);
        } else if (matches(s, "Å¡")) {
            set.add(CZECH);
        } else if (matches(s, "Å¥")) {
            set.add(CZECH);
        } else if (matches(s, "Ä›")) {
            set.add(CZECH);
        } else if (matches(s, "Å¯")) {
            set.add(CZECH);
        }
        
        if (matches(s, "Ãª")) {
            set.add(FRENCH);
        } else if (matches(s, "Ã¹")) {
            set.add(FRENCH);
        }
        
        if (matches(s, "ÃŸ")) {
            set.add(GERMAN);
        } else if (matches(s, "Ã¤")) {
            set.add(GERMAN);
        }
        
        if (matches(s, "Å±")) {
            set.add(HUNGARIAN);
        }
        
        if (matches(s, "Ä‡")) {
            set.add(POLISH);
        } else if (matches(s, "Å‚")) {
            set.add(POLISH);
        } else if (matches(s, "Å„")) {
            set.add(POLISH);
        } else if (matches(s, "Å›")) {
            set.add(POLISH);
        } else if (matches(s, "Åº")) {
            set.add(POLISH);
        } else if (matches(s, "Å¼")) {
            set.add(POLISH);
        } else if (matches(s, "Ä…")) {
            set.add(POLISH);
        } else if (matches(s, "Ä™")) {
            set.add(POLISH);
        }
        
        if (matches(s, "Ã ")) {
            set.add(PORTUGUESE);
        } else if (matches(s, "Ã£")) {
            set.add(PORTUGUESE);
        }
        
        if (matches(s, "Å£")) {
            set.add(ROMANIAN);
        } else if (matches(s, "Äƒ")) {
            set.add(ROMANIAN);
        }
        
        if (matches(s, "Ã±")) {
            set.add(SPANISH);
        }
        
        if (matches(s, "ÄŸ")) {
            set.add(TURKISH);
        } else if (matches(s, "Ä±")) {
            set.add(TURKISH);
        }
        
        if (matches(s, "Ã§")) {
            set.add(FRENCH);
            set.add(SPANISH);
            set.add(PORTUGUESE);
            set.add(TURKISH);
        }
        if (matches(s, "ÅŸ")) {
            set.add(ROMANIAN);
            set.add(TURKISH);
        }
        if (matches(s, "Ã¡")) {
            set.add(HUNGARIAN);
            set.add(SPANISH);
            set.add(PORTUGUESE);
            set.add(CZECH);
            set.add(GREEKLATIN);
        }
        if (matches(s, "Ã¢")) {
            set.add(ROMANIAN);
            set.add(FRENCH);
            set.add(PORTUGUESE);
        }
        if (matches(s, "Ã©")) {
            set.add(FRENCH);
            set.add(HUNGARIAN);
            set.add(CZECH);
            set.add(GREEKLATIN);
        }
        if (matches(s, "Ã¨")) {
            set.add(FRENCH);
            set.add(SPANISH);
            set.add(ITALIAN);
        }
        if (matches(s, "Ãª")) {
            set.add(FRENCH);
            set.add(PORTUGUESE);
        }
        if (matches(s, "Ã­")) {
            set.add(HUNGARIAN);
            set.add(SPANISH);
            set.add(PORTUGUESE);
            set.add(CZECH);
            set.add(GREEKLATIN);
        }
        if (matches(s, "Ã®")) {
            set.add(ROMANIAN);
            set.add(FRENCH);
        }
        if (matches(s, "Ã³")) {
            set.add(POLISH);
            set.add(HUNGARIAN);
            set.add(SPANISH);
            set.add(ITALIAN);
            set.add(PORTUGUESE);
            set.add(CZECH);
            set.add(GREEKLATIN);
        }
        if (matches(s, "Ã¶")) {
            set.add(GERMAN);
            set.add(HUNGARIAN);
            set.add(TURKISH);
        }
        if (matches(s, "Ã´")) {
            set.add(FRENCH);
            set.add(PORTUGUESE);
        }
        if (matches(s, "Ãµ")) {
            set.add(PORTUGUESE);
            set.add(HUNGARIAN);
        }
        if (matches(s, "Ã²")) {
            set.add(ITALIAN);
            set.add(SPANISH);
        }
        if (matches(s, "Ãº")) {
            set.add(HUNGARIAN);
            set.add(SPANISH);
            set.add(PORTUGUESE);
            set.add(CZECH);
            set.add(GREEKLATIN);
        }
        if (matches(s, "Ã¼")) {
            set.add(GERMAN);
            set.add(HUNGARIAN);
            set.add(SPANISH);
            set.add(PORTUGUESE);
            set.add(TURKISH);
        }
        if (matches(s, "Ã½")) {
            set.add(CZECH);
            set.add(GREEKLATIN);
        }

        // Every Cyrillic word has at least one Cyrillic vowel (Ð°Ñ‘ÐµÐ¾Ð¸ÑƒÑ‹Ñ�ÑŽÑ�) 
        if (matches(s, "Ð°")) {
            set.add(CYRILLIC);
        } else if (matches(s, "Ñ‘")) {
            set.add(CYRILLIC);
        } else if (matches(s, "Ð¾")) {
            set.add(CYRILLIC);
        } else if (matches(s, "Ðµ")) {
            set.add(CYRILLIC);
        } else if (matches(s, "Ð¸")) {
            set.add(CYRILLIC);
        } else if (matches(s, "Ñƒ")) {
            set.add(CYRILLIC);
        } else if (matches(s, "Ñ‹")) {
            set.add(CYRILLIC);
        } else if (matches(s, "Ñ�")) {
            set.add(CYRILLIC);
        } else if (matches(s, "ÑŽ")) {
            set.add(CYRILLIC);
        } else if (matches(s, "Ñ�")) {
            set.add(CYRILLIC);
        }

        // Every Greek word has at least one Greek vowel
        if (matches(s, "Î±")) {
            set.add(GREEK);
        } else if (matches(s, "Îµ")) {
            set.add(GREEK);
        } else if (matches(s, "Î·")) {
            set.add(GREEK);
        } else if (matches(s, "Î¹")) {
            set.add(GREEK);
        } else if (matches(s, "Î¿")) {
            set.add(GREEK);
        } else if (matches(s, "Ï…")) {
            set.add(GREEK);
        } else if (matches(s, "Ï‰")) {
            set.add(GREEK);
        }

        // Arabic (only initial)
        if (matches(s, "Ø§")) {
            set.add(ARABIC); // alif (isol + init)
        } else if (matches(s, "Ø¨")) {
            set.add(ARABIC); // ba' 
        } else if (matches(s, "Øª")) {
            set.add(ARABIC); // ta' 
        } else if (matches(s, "Ø«")) {
            set.add(ARABIC); // tha'
        } else if (matches(s, "Ø¬")) {
            set.add(ARABIC); // jim
        } else if (matches(s, "Ø­")) {
            set.add(ARABIC); // h.a' 
        } else if (matches(s, "Ø®'")) {
            set.add(ARABIC); // kha' 
        } else if (matches(s, "Ø¯")) {
            set.add(ARABIC); // dal (isol + init)
        } else if (matches(s, "Ø°")) {
            set.add(ARABIC); // dhal (isol + init)
        } else if (matches(s, "Ø±")) {
            set.add(ARABIC); // ra' (isol + init)
        } else if (matches(s, "Ø²")) {
            set.add(ARABIC); // za' (isol + init)
        } else if (matches(s, "Ø³")) {
            set.add(ARABIC); // sin 
        } else if (matches(s, "Ø´")) {
            set.add(ARABIC); // shin 
        } else if (matches(s, "Øµ")) {
            set.add(ARABIC); // s.ad 
        } else if (matches(s, "Ø¶")) {
            set.add(ARABIC); // d.ad 
        } else if (matches(s, "Ø·")) {
            set.add(ARABIC); // t.a' 
        } else if (matches(s, "Ø¸")) {
            set.add(ARABIC); // z.a' 
        } else if (matches(s, "Ø¹")) {
            set.add(ARABIC); // 'ayn
        } else if (matches(s, "Øº")) {
            set.add(ARABIC); // ghayn 
        } else if (matches(s, "Ù�")) {
            set.add(ARABIC); // fa' 
        } else if (matches(s, "Ù‚")) {
            set.add(ARABIC); // qaf 
        } else if (matches(s, "Ùƒ")) {
            set.add(ARABIC); // kaf  
        } else if (matches(s, "Ù„")) {
            set.add(ARABIC); // lam 
        } else if (matches(s, "Ù…")) {
            set.add(ARABIC); // mim 
        } else if (matches(s, "Ù†")) {
            set.add(ARABIC); // nun 
        } else if (matches(s, "Ù‡")) {
            set.add(ARABIC); // ha' 
        } else if (matches(s, "Ùˆ")) {
            set.add(ARABIC); // waw (isol + init)
        } else if (matches(s, "ÙŠ")) {
            set.add(ARABIC); // ya' 
        } else if (matches(s, "Ø¢")) {
            set.add(ARABIC); // alif madda
        } else if (matches(s, "Ø¥")) {
            set.add(ARABIC); // alif + diacritic 
        } else if (matches(s, "Ø£")) {
            set.add(ARABIC); // alif + hamza
        } else if (matches(s, "Ø¤")) {
            set.add(ARABIC); //  waw + hamza
        } else if (matches(s, "Ø¦")) {
            set.add(ARABIC); //  ya' + hamza
        } /*else if(matches(s, "Ù„Ø§/â€Ž")) {
            set.add(ARABIC); // ligature l+a}
        } */

        // Hebrew 
        if (matches(s, "×�")) {
            set.add(HEBREW);
        } else if (matches(s, "×‘")) {
            set.add(HEBREW);
        } else if (matches(s, "×’")) {
            set.add(HEBREW);
        } else if (matches(s, "×“")) {
            set.add(HEBREW);
        } else if (matches(s, "×”")) {
            set.add(HEBREW);
        } else if (matches(s, "×•")) {
            set.add(HEBREW);
        } else if (matches(s, "×–")) {
            set.add(HEBREW);
        } else if (matches(s, "×—")) {
            set.add(HEBREW);
        } else if (matches(s, "×˜")) {
            set.add(HEBREW);
        } else if (matches(s, "×™")) {
            set.add(HEBREW);
        } else if (matches(s, "×›")) {
            set.add(HEBREW);
        } else if (matches(s, "×œ")) {
            set.add(HEBREW);
        } else if (matches(s, "×ž")) {
            set.add(HEBREW);
        } else if (matches(s, "× ")) {
            set.add(HEBREW);
        } else if (matches(s, "×¡")) {
            set.add(HEBREW);
        } else if (matches(s, "×¢")) {
            set.add(HEBREW);
        } else if (matches(s, "×¤")) {
            set.add(HEBREW);
        } else if (matches(s, "×¦")) {
            set.add(HEBREW);
        } else if (matches(s, "×§")) {
            set.add(HEBREW);
        } else if (matches(s, "×¨")) {
            set.add(HEBREW);
        } else if (matches(s, "×©")) {
            set.add(HEBREW);
        } else if (matches(s, "×ª")) {
            set.add(HEBREW);
        }

        // 2. following are rules to reject the language
        // Every Latin character word has at least one Latin vowel  
        if (matches(s, "[aeioy]")) {
            set.remove(CYRILLIC);
            set.remove(HEBREW);
            set.remove(GREEK);
            set.remove(ARABIC);
        }
        if (matches(s, "y")) {
            set.remove(CYRILLIC);
            set.remove(HEBREW);
            set.remove(GREEK);
            set.remove(ARABIC);
            set.remove(ROMANIAN);
            set.remove(DUTCH);
        }

        if (matches(s, "g")) {
            set.remove(CZECH);
        }
        if (matches(s, "sc[ei]")) {
            set.remove(FRENCH);
        }
        
        if (matches(s, "son$")) {
            set.remove(GERMAN);
        } else if (matches(s, "v[^aoeiu]")) {
            // in german, "v" can be found before a vowel only
            set.remove(GERMAN);
        } else if (matches(s, "y[^aoeiu]")) {
            // in german, "y" usually appears only in the last position; sometimes before a vowel
            set.remove(GERMAN);
        } else if (matches(s, "c[^aohk]")) {
            set.remove(GERMAN);
        } else if (matches(s, "ou")) {
            set.remove(GERMAN);
        }
        
        if (matches(s, "j")) {
            set.remove(ITALIAN);
        } else if (matches(s, "ch[aou]")) {
            set.remove(ITALIAN);
        }
        
        if (matches(s, "v")) {
            set.remove(POLISH);
        } else if (matches(s, "ky")) {
            set.remove(POLISH);
        }
        
        if (matches(s, "^h")) {
            set.remove(RUSSIAN);
        }
        if (matches(s, "ch")) {
            set.remove(TURKISH);
        } else if (matches(s, "a[eiou]")) {
            // no diphthongs in Turkish
            set.remove(TURKISH);
        } else if (matches(s, "Ã¶[eaiou]")) {
            set.remove(TURKISH);
        } else if (matches(s, "Ã¼[eaiou]")) {
            set.remove(TURKISH);
        } else if (matches(s, "e[aiou]")) {
            set.remove(TURKISH);
        } else if (matches(s, "i[aeou]")) {
            set.remove(TURKISH);
        } else if (matches(s, "o[aieu]")) {
            set.remove(TURKISH);
        } else if (matches(s, "u[aieo]")) {
            set.remove(TURKISH);
        }
        
        
        if (matches(s, "j[^aoeiuy]")) {
            set.remove(FRENCH);
            set.remove(SPANISH);
            set.remove(PORTUGUESE);
            set.remove(GREEKLATIN);
        }
        if (matches(s, "k")) {
            set.remove(ROMANIAN);
            set.remove(SPANISH);
            set.remove(PORTUGUESE);
            set.remove(FRENCH);
            set.remove(ITALIAN);
        }
        if (matches(s, "q")) {
            set.remove(HUNGARIAN);
            set.remove(POLISH);
            set.remove(RUSSIAN);
            set.remove(ROMANIAN);
            set.remove(CZECH);
            set.remove(DUTCH);
            set.remove(TURKISH);
            set.remove(GREEKLATIN);
        }
        if (matches(s, "w")) {
            set.remove(FRENCH);
            set.remove(ROMANIAN);
            set.remove(SPANISH);
            set.remove(HUNGARIAN);
            set.remove(RUSSIAN);
            set.remove(CZECH);
            set.remove(TURKISH);
            set.remove(GREEKLATIN);
        }
        if (matches(s, "x")) {
            // polish excluded from the list
            set.remove(CZECH);
            set.remove(HUNGARIAN);
            set.remove(DUTCH);
            set.remove(TURKISH);
        } 
        if (matches(s, "dj")) {
            set.remove(SPANISH);
            set.remove(TURKISH);
        }
        if (matches(s, "dzi")) {
            set.remove(GERMAN);
            set.remove(ENGLISH);
            set.remove(FRENCH);
            set.remove(TURKISH);
        }
        if (matches(s, "(aj|ej|oj|uj)")) {
            set.remove(GERMAN);
            set.remove(ENGLISH);
            set.remove(FRENCH);
            set.remove(DUTCH);
        }
        if (matches(s, "eu")) {
            set.remove(RUSSIAN);
            set.remove(POLISH);
        }
        if (matches(s, "kie")) {
            set.remove(FRENCH);
            set.remove(SPANISH);
            set.remove(GREEKLATIN);
        }
        if (matches(s, "gie")) {
            set.remove(PORTUGUESE);
            set.remove(ROMANIAN);
            set.remove(SPANISH);
            set.remove(GREEKLATIN);
        }
        if (matches(s, "sch")) {
            set.remove(HUNGARIAN);
            set.remove(POLISH);
            set.remove(FRENCH);
            set.remove(SPANISH);
        }
        

        if (set.isEmpty()) {
            result = ANY;
        }
/*        else if (set.size() > 1)
            result = ANY;*/
        else {
            Iterator<String> it = set.iterator();
            result += it.next();
        }
        return result;
    }

    // FIXME: Change the characters to UTF-8 enitiy Ref's (use CovertData2UTF8 class)
    private String identiyASHLanguage(String s){
        String result = "";
        Set<String> set = new LinkedHashSet<String>();
        
        // 1. following are rules to accept the language
        // 1.1 Special letter combinations
        if(matches(s, "zh")) {
            set.add(POLISH);
            set.add(RUSSIAN);
            set.add(GERMAN);
            set.add(ENGLISH);
        }
        if(matches(s, "eau")) set.add(FRENCH);
        if(matches(s, "[aoeiuäöü]h")) set.add(GERMAN);
        if(matches(s, "^vogel")) set.add(GERMAN);
        if(matches(s, "vogel$")) set.add(GERMAN);
        if(matches(s, "witz")) set.add(GERMAN);
        if(matches(s, "tz$")) {
            set.add(GERMAN);
            set.add(RUSSIAN);
            set.add(ENGLISH);
        }
        if(matches(s, "^tz")) {
            set.add(RUSSIAN);
            set.add(ENGLISH);
        }
        if(matches(s, "güe")) set.add(SPANISH);
        if(matches(s, "güi")) set.add(SPANISH);
        if(matches(s, "ghe")) set.add(ROMANIAN);
        if(matches(s, "ghi")) set.add(ROMANIAN);
        if(matches(s, "vici$")) set.add(ROMANIAN);
        if(matches(s, "schi$")) set.add(ROMANIAN);
        if(matches(s, "chsch")) set.add(GERMAN);
        if(matches(s, "tsch")) set.add(GERMAN);
        if(matches(s, "ssch")) set.add(GERMAN);
        if(matches(s, "sch$")) {
            set.add(GERMAN);
            set.add(RUSSIAN);
        }
        if(matches(s, "^sch")) {
            set.add(GERMAN);
            set.add(RUSSIAN);
        }
        if(matches(s, "^rz")) set.add(POLISH);
        if(matches(s, "rz$")) {
            set.add(POLISH);
            set.add(GERMAN);
        }
        if(matches(s, "[^aoeiuäöü]rz")) set.add(POLISH);
        if(matches(s, "rz[^aoeiuäöü]")) set.add(POLISH);
        if(matches(s, "cki$")) set.add(POLISH);
        if(matches(s, "ska$")) set.add(POLISH);
        if(matches(s, "cka$")) set.add(POLISH);
        if(matches(s, "ue")) {
            set.add(GERMAN);
            set.add(RUSSIAN);
        }
        if(matches(s, "ae")) {
            set.add(GERMAN);
            set.add(RUSSIAN);
            set.add(ENGLISH);
        }
        if(matches(s, "oe")) {
            set.add(GERMAN);
            set.add(FRENCH);
            set.add(RUSSIAN);
            set.add(ENGLISH);
        }
        if(matches(s, "th$")) set.add(GERMAN);
        if(matches(s, "^th")) set.add(GERMAN);
        if(matches(s, "th[^aoeiu]")) set.add(GERMAN);
        if(matches(s, "mann")) set.add(GERMAN);
        if(matches(s, "cz")) set.add(POLISH);
        if(matches(s, "cy")) set.add(POLISH);
        if(matches(s, "niew")) set.add(POLISH);
        if(matches(s, "stein")) set.add(GERMAN);
        if(matches(s, "heim$")) set.add(GERMAN);
        if(matches(s, "heimer$")) set.add(GERMAN);
        if(matches(s, "ii$")) set.add(RUSSIAN);
        if(matches(s, "iy$")) set.add(RUSSIAN);
        if(matches(s, "yy$")) set.add(RUSSIAN);
        if(matches(s, "yi$")) set.add(RUSSIAN);
        if(matches(s, "yj$")) set.add(RUSSIAN);
        if(matches(s, "ij$")) set.add(RUSSIAN);
        if(matches(s, "gaus$")) set.add(RUSSIAN);
        if(matches(s, "gauz$")) set.add(RUSSIAN);
        if(matches(s, "gauz$")) set.add(RUSSIAN);
        if(matches(s, "goltz$")) set.add(RUSSIAN);
        if(matches(s, "gol'tz$")) set.add(RUSSIAN);
        if(matches(s, "golts$")) set.add(RUSSIAN);
        if(matches(s, "gol'ts$")) set.add(RUSSIAN); 
        if(matches(s, "^goltz")) set.add(RUSSIAN);
        if(matches(s, "^gol'tz")) set.add(RUSSIAN);
        if(matches(s, "^golts")) set.add(RUSSIAN);
        if(matches(s, "^gol'ts")) set.add(RUSSIAN);
        if(matches(s, "gendler$")) set.add(RUSSIAN);
        if(matches(s, "gejmer$")) set.add(RUSSIAN);
        if(matches(s, "gejm$")) set.add(RUSSIAN);
        if(matches(s, "geimer$")) set.add(RUSSIAN);
        if(matches(s, "geim$")) set.add(RUSSIAN);
        if(matches(s, "geymer")) set.add(RUSSIAN);
        if(matches(s, "geym$")) set.add(RUSSIAN);
        if(matches(s, "gof$")) set.add(RUSSIAN);
        if(matches(s, "thal")) set.add(GERMAN);
        if(matches(s, "zweig")) set.add(GERMAN);
        if(matches(s, "ck$")) {
            set.add(GERMAN);
            set.add(ENGLISH);
        }
        if(matches(s, "c$")) {
            set.add(POLISH);
            set.add(ROMANIAN);
            set.add(HUNGARIAN);
        }
        if(matches(s, "sz")) {
            set.add(POLISH);
            set.add(HUNGARIAN);
        }
        if(matches(s, "gue")) {
            set.add(SPANISH);
            set.add(FRENCH);
        }
        if(matches(s, "gui")) {
            set.add(SPANISH);
            set.add(FRENCH);
        }
        if(matches(s, "guy")) set.add(FRENCH);
        if(matches(s, "cs$")) set.add(HUNGARIAN);
        if(matches(s, "^cs")) set.add(HUNGARIAN);
        if(matches(s, "dzs")) set.add(HUNGARIAN);
        if(matches(s, "zs$")) set.add(HUNGARIAN);
        if(matches(s, "^zs")) set.add(HUNGARIAN);
        if(matches(s, "^wl")) set.add(POLISH);
        if(matches(s, "^wr")) {
            set.add(POLISH);
            set.add(ENGLISH);
            set.add(GERMAN);
        }

        if(matches(s, "gy$")) set.add(HUNGARIAN);
        if(matches(s, "gy[aeou]")) set.add(HUNGARIAN);
        if(matches(s, "gy")) {
            set.add(HUNGARIAN);
            set.add(RUSSIAN);
        }
        if(matches(s, "(ly|ny|ty)")) {
            set.add(HUNGARIAN);
            set.add(RUSSIAN);
            set.add(POLISH);
        }

        // 1.2 special characters    
        if(matches(s, "â")) {
            set.add(ROMANIAN);
            set.add(FRENCH);
        }
        if(matches(s, "ă")) 
            set.add(ROMANIAN);
        if(matches(s, "à")) set.add(FRENCH);
        if(matches(s, "ä")) set.add(GERMAN);
        if(matches(s, "á")) {
            set.add(HUNGARIAN);
            set.add(SPANISH);
        }
        if(matches(s, "ą")) set.add(POLISH);
        if(matches(s, "ć")) set.add(POLISH);
        if(matches(s, "ç")) set.add(FRENCH);
        if(matches(s, "ę")) set.add(POLISH);
        if(matches(s, "é")) {
            set.add(FRENCH);
            set.add(HUNGARIAN);
            set.add(SPANISH);
        }
        if(matches(s, "è")) set.add(FRENCH);
        if(matches(s, "ê")) set.add(FRENCH);
        if(matches(s, "í")) {
            set.add(HUNGARIAN);
            set.add(SPANISH);
        }
        if(matches(s, "î")) {
            set.add(ROMANIAN);
            set.add(FRENCH);
        }
        if(matches(s, "ł")) set.add(POLISH);
        if(matches(s, "ń")) set.add(POLISH);
        if(matches(s, "ñ")) set.add(SPANISH);
        if(matches(s, "ó")) {
            set.add(POLISH);
            set.add(HUNGARIAN);
            set.add(SPANISH);
        }
        if(matches(s, "ö")) {
            set.add(GERMAN);
            set.add(HUNGARIAN);
        }
        if(matches(s, "õ")) set.add(HUNGARIAN);
        if(matches(s, "ş")) set.add(ROMANIAN);
        if(matches(s, "ś")) set.add(POLISH);
        if(matches(s, "ţ")) set.add(ROMANIAN);
        if(matches(s, "ü")) {
            set.add(GERMAN);
            set.add(HUNGARIAN);
        }
        if(matches(s, "ù")) set.add(FRENCH);
        if(matches(s, "ű")) set.add(HUNGARIAN);
        if(matches(s, "ú")) {
            set.add(HUNGARIAN);
            set.add(SPANISH);
        }
        if(matches(s, "ź")) set.add(POLISH);
        if(matches(s, "ż")) set.add(POLISH);
        if(matches(s, "ß")) set.add(GERMAN);

        // Every Cyrillic word has at least one Cyrillic vowel (аёеоиуыэюя) 
        if(matches(s, "[аёоеиуыэюя]")) set.add(CYRILLIC);

        // Hebrew 
        if(matches(s, "א")) set.add(HEBREW);
        else if(matches(s, "ב")) set.add(HEBREW);
        else if(matches(s, "ג")) set.add(HEBREW);
        else if(matches(s, "ד")) set.add(HEBREW);
        else if(matches(s, "ה")) set.add(HEBREW);
        else if(matches(s, "ו")) set.add(HEBREW);
        else if(matches(s, "ז")) set.add(HEBREW);
        else if(matches(s, "ח")) set.add(HEBREW);
        else if(matches(s, "ט")) set.add(HEBREW);
        else if(matches(s, "י")) set.add(HEBREW);
        else if(matches(s, "כ")) set.add(HEBREW);
        else if(matches(s, "ל")) set.add(HEBREW);
        else if(matches(s, "מ")) set.add(HEBREW);
        else if(matches(s, "נ")) set.add(HEBREW);
        else if(matches(s, "ס")) set.add(HEBREW);
        else if(matches(s, "ע")) set.add(HEBREW);
        else if(matches(s, "פ")) set.add(HEBREW);
        else if(matches(s, "צ")) set.add(HEBREW);
        else if(matches(s, "ק")) set.add(HEBREW);
        else if(matches(s, "ר")) set.add(HEBREW);
        else if(matches(s, "ש")) set.add(HEBREW);
        else if(matches(s, "ת")) set.add(HEBREW);

        // 2. following are rules to reject the language    
        // Every Latin character word has at least one Latin vowel  
        if(!matches(s, "[aeiou]")) {
            set.remove(CYRILLIC);
            set.remove(HEBREW);
        }
        
        if(!matches(s, "y")) {
            set.remove(CYRILLIC);
            set.remove(HEBREW);
            set.remove(ROMANIAN);
        }
        if(matches(s, "v[^aoeiuäüö]")) {
            // in german, "v" can be found before a vowel only
            set.remove(GERMAN);
        }
        if(matches(s, "y[^aoeiu]")) {
            // in german, "y" usually appears only in the last position; sometimes before a vowel
            set.remove(GERMAN);
        }
        if(matches(s, "c[^aohk]")) {
            set.remove(GERMAN);
        }
        if(matches(s, "dzi")) {
            set.remove(GERMAN);
            set.remove(ENGLISH);
            set.remove(FRENCH);
        }
        if(matches(s, "ou")) {
            set.remove(GERMAN);
        }
        if(matches(s, "(aj|ej|oj|uj)")) {
            set.remove(GERMAN);
            set.remove(ENGLISH);
            set.remove(FRENCH);
        }
        if(matches(s, "k")) {
            set.remove(ROMANIAN);
        }
        if(matches(s, "v")) {
            set.remove(POLISH);
        }
        if(matches(s, "ky")) {
            set.remove(POLISH);
        } 
        if(matches(s, "eu")) {
            set.remove(RUSSIAN);
            set.remove(POLISH);
        } 
        if(matches(s, "w")) {
            set.remove(FRENCH);
            set.remove(ROMANIAN);
            set.remove(SPANISH);
            set.remove(HUNGARIAN);
            set.remove(RUSSIAN);
        } 
        if(matches(s, "kie")) {
            set.remove(FRENCH);
            set.remove(SPANISH);
        } 
        if(matches(s, "gie")) {
            set.remove(FRENCH);
            set.remove(ROMANIAN);
            set.remove(SPANISH);
        } 
        if(matches(s, "q")) {
            set.remove(HUNGARIAN);
            set.remove(POLISH);
            set.remove(RUSSIAN);
            set.remove(ROMANIAN);
        } 
        if(matches(s, "sch")) {
            set.remove(HUNGARIAN);
            set.remove(POLISH);
            set.remove(FRENCH);
            set.remove(SPANISH);
        } 
        if(matches(s, "^h")) {
                set.remove(RUSSIAN);
        }
        
        if (set.isEmpty())
            result = ANY;
/*        else if (set.size() > 1)
            result = ANY;*/
        else {
            Iterator<String> it = set.iterator();
            result += it.next();
        }
        return result;
    }

    // FIXME: Change the characters to UTF-8 enitiy Ref's (use CovertData2UTF8 class)
    private String identiySEPLanguage(String s){
        String result = "";
        Set<String> set = new LinkedHashSet<String>();
        
        
        if (set.isEmpty())
            result = ANY;
/*        else if (set.size() > 1)
            result = ANY;*/
        else {
            Iterator<String> it = set.iterator();
            result += it.next();
        }
        return result;
    }
    
    private boolean matches(String s, String regex){
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(s);
        return m.find();
    }
    
    //==========================================================================
    // Step 2: Calculating the Exact Phonetic Value
    //==========================================================================
    
    //==========================================================================
    // Step 3: Calculating the Approximate Phonetic Value
    //==========================================================================
    
    //==========================================================================
    // Step 4: Calculating the Hebrew Phonetic Value
    //==========================================================================

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        BeiderMorseSoundex bms = new BeiderMorseSoundex();
        System.out.format("%1$12s %2$12s %3$12s%n", "Renault", "french", bms.identiyLanguage("Renault"));
        
        System.out.format("%1$12s %2$12s %3$12s%n", "Mickiewicz", "polish", bms.identiyLanguage("Mickiewicz"));
        // this also hits german and greeklatin
        System.out.format("%1$12s %2$12s %3$12s%n", "Thompson", "english", bms.identiyLanguage("Thompson"));
        // NuÃ±ez
        System.out.format("%1$12s %2$12s %3$12s%n", "Nu\u00f1ez", "spanish", bms.identiyLanguage("Nu\u00f1ez"));
        System.out.format("%1$12s %2$12s %3$12s%n", "Carvalho", "portuguese", bms.identiyLanguage("Carvalho"));
        // ÄŒapek
        System.out.format("%1$12s %2$12s %3$12s%n", "\u010capek", "czech", bms.identiyLanguage("\u010capek"));
        System.out.format("%1$12s %2$12s %3$12s%n", "Sjneijder", "dutch", bms.identiyLanguage("Sjneijder"));
        System.out.format("%1$12s %2$12s %3$12s%n", "Klausewitz", "german", bms.identiyLanguage("Klausewitz"));
        // KÃ¼Ã§Ã¼k
        System.out.format("%1$12s %2$12s %3$12s%n", "K\u00fc\u00e7\u00fck", "turkish", bms.identiyLanguage("K\u00fc\u00e7\u00fck"));
        System.out.format("%1$12s %2$12s %3$12s%n", "Giacometti", "italian", bms.identiyLanguage("Giacometti"));
        System.out.format("%1$12s %2$12s %3$12s%n", "Nagy", "hungarian", bms.identiyLanguage("Nagy"));
        // CeauÅŸescu
        System.out.format("%1$12s %2$12s %3$12s%n", "Ceau\u015fescu", "romanian", bms.identiyLanguage("Ceau\u015fescu"));
        System.out.format("%1$12s %2$12s %3$12s%n", "Angelopoulos", "greeklatin", bms.identiyLanguage("Angelopoulos"));
        // Î‘Î³Î³ÎµÎ»ÏŒÏ€Î¿Ï…Î»Î¿Ï‚
        System.out.format("%1$12s %2$12s %3$12s%n", "\u0391\u03b3\u03b3\u03b5\u03bb\u03cc\u03c0\u03bf\u03c5\u03bb\u03bf\u03c2", "greek", bms.identiyLanguage("\u0391\u03b3\u03b3\u03b5\u03bb\u03cc\u03c0\u03bf\u03c5\u03bb\u03bf\u03c2"));
        // ÐŸÑƒÑˆÐºÐ¸Ð½
        System.out.format("%1$12s %2$12s %3$12s%n", "\u041f\u0443\u0448\u043a\u0438\u043d", "cyrillic", bms.identiyLanguage("\u041f\u0443\u0448\u043a\u0438\u043d"));
        // ×›×”×Ÿ
        System.out.format("%1$12s %2$12s %3$12s%n", "\u05db\u05d4\u05df", "hebrew", bms.identiyLanguage("\u05db\u05d4\u05df"));
        // Ã¡cz
        System.out.format("%1$12s %2$12s %3$12s%n", "\u00e1cz", "any", bms.identiyLanguage("\u00e1cz"));
        // Ã¡tz
        System.out.format("%1$12s %2$12s %3$12s%n", "\u00e1tz", "any", bms.identiyLanguage("\u00e1tz"));
    }
}
