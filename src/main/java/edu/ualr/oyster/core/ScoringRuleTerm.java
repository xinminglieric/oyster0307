/*
 * Copyright 2012 John Talburt, Eric Nelson
 *
 * This file is part of Oyster created in the ERIQ Research Center at University of Arkansas at Little Rock.
 * 
 * Oyster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Oyster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Oyster.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package edu.ualr.oyster.core;

/**
 * ScoringRuleTerm.java Created on June 5th, 2013 9:58:16 PM
 * 
 * @author Cheng Chen
 */
public class ScoringRuleTerm {
	private String item = null;
	private String similarity = null;
	private String dataprep = null;
	private int agreewgt = 0;
	private String wgttable = null;
	private int disagreewgt = 0;
	private int missing = 0;
	private boolean hasmissingweight=false;
	
	/**
	 * Creates a new instance of RuleTerm
	 */
	public ScoringRuleTerm() {
	}
	
	public String getItem() {
		return item;
	}
	
	public void setItem(String item) {
		this.item = item;
	}
	
	public String getSimilarity() {
		return similarity;
	}
	
	public void setSimilarity(String similarity) {
		this.similarity = similarity;
	}
	
	public String getDataprep() {
		return dataprep;
	}
	
	public void setDataprep(String dataprep) {
		this.dataprep = dataprep;
	}
	
	public int getAgreewgt() {
		return agreewgt;
	}
	
	public void setAgreewgt(int agreewgt) {
		this.agreewgt = agreewgt;
	}
	
	public String getWgttable() {
		return wgttable;
	}
	
	public void setWgttable(String wgttable) {
		this.wgttable = wgttable;
	}
	
	public int getDisagreewgt() {
		return disagreewgt;
	}
	
	public void setDisagreewgt(int disagreewgt) {
		this.disagreewgt = disagreewgt;
	}
	
	public int getMissing() {
		return missing;
	}
	
	public void setMissing(int missing) {
		this.missing = missing;
	}
	
	public boolean isHasmissingweight() {
		return hasmissingweight;
	}

	public void setHasmissingweight(boolean hasmissingweight) {
		this.hasmissingweight = hasmissingweight;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final ScoringRuleTerm other = (ScoringRuleTerm) obj;
		if (this.item == null || !this.item.equals(other.item)) {
			return false;
		}
		if (this.similarity == null || !this.similarity.equals(other.similarity)) {
			return false;
		}
		//because dataprep is optional
		if (!this.dataprep.equals(other.dataprep)) {
			return false;
		}
		if (this.agreewgt!=other.agreewgt) {
			return false;
		}
		//weight table is optional
		if (!this.wgttable.equals(other.wgttable)) {
			return false;
		}
		if (this.disagreewgt!=other.disagreewgt) {
			return false;
		}
		if(this.hasmissingweight!= other.hasmissingweight){
			return false;
		}
		else{
			if (this.missing!=other.missing) {
				return false;
			}
		}
		return true;
	}
	
	@Override
	public int hashCode() {
		int hash = 7;
		hash = 17 * hash + (this.item != null ? this.item.hashCode() : 0);
		hash = 17 * hash + (this.similarity != null ? this.similarity.hashCode() : 0);
		hash = 17 * hash + (this.dataprep != null ? this.dataprep.hashCode() : 0);
		hash = 17 * hash + this.agreewgt;
		hash = 17 * hash + (this.wgttable != null ? this.wgttable.hashCode() : 0);
		hash = 17 * hash + this.disagreewgt;
		hash = 17 * hash + this.missing;
		return hash;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getClass().getName());
		sb.append("[item=");
		sb.append(this.item != null ? this.item : "");
		sb.append(", similarity=");
		sb.append(this.similarity != null ? this.similarity : "");
		sb.append(", dataprep=");
		sb.append(this.dataprep != null ? this.dataprep : "");
		sb.append(", agreewgt=");
		sb.append(this.agreewgt);
		sb.append(", weightTable=");
		sb.append(this.wgttable != null ? this.wgttable : "");
		sb.append(", disagreewgt=");
		sb.append(this.disagreewgt);
		if(hasmissingweight){
			sb.append(", missing=");
			sb.append(this.missing);
		}
		sb.append("]");
		return sb.toString();
	}
}
