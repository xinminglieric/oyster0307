package edu.ualr.oyster.data;

/**
 *Author: Cheng Chen
 *Date: Apr 29, 2014
 *Function:
 */

public class TrueNegativeAttributes {
	private String runId;
	private String reviewer;
	private String transaction;
	private String type;
	
//	public TrueNegativeAttributes() {
//	      this.runId = runId;
//      }
	
	public String getRunId() {
		return runId;
	}
	public void setRunId(String runId) {
		this.runId = runId;
	}
	public String getReviewer() {
		return reviewer;
	}
	public void setReviewer(String reviewer) {
		this.reviewer = reviewer;
	}
	public String getTransaction() {
		return transaction;
	}
	public void setTransaction(String transaction) {
		this.transaction = transaction;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof TrueNegativeAttributes){
			TrueNegativeAttributes objtp=(TrueNegativeAttributes) obj;
			if((this.reviewer.equals(objtp.reviewer))&&(this.runId.equals(objtp.runId))&&(this.transaction.equals(objtp.transaction))
					&&(this.type.equals(objtp.type))){
				return true;
			}
			else return false;
		}
		else return false;
	}
	
	@Override
	public String toString(){
		StringBuffer sb=new StringBuffer();
		
		
		if(this.runId!=null){
			sb.append("RunID=\""+this.runId+"\" ");
		}
		else{
			sb.append("RunID=\"\" ");
		}
		
		if(this.reviewer!=null){
			sb.append("Reviewer=\""+this.reviewer+"\" ");
		}
		else{
			sb.append("Reviewer=\"\" ");
		}
		
		if(this.transaction!=null){
			sb.append("Transaction=\""+this.transaction+"\" ");
		}
		else{
			sb.append("Transaction=\"\" ");
		}
		
		if(this.type!=null){
			sb.append("Type=\""+this.type+"\"");
		}
		else{
			sb.append("Type=\"\"");
		}
		return sb.toString();
	}
	
	@Override
	    public int hashCode() {
	        int hash = 5;
	        hash = 59 * hash + (this.runId != null ? this.runId.hashCode() : 0);
	        hash = 59 * hash + (this.reviewer != null ? this.reviewer.hashCode() : 0);
	        hash = 59 * hash + (this.transaction != null ? this.transaction.hashCode() : 0);
	        hash = 59 * hash + (this.transaction != null ? this.type.hashCode() : 0);
	        return hash;
	    }
	
}
