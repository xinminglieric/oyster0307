package edu.ualr.oyster.data;

/**
 *Author: Cheng Chen
 *Date: Apr 17, 2014
 *Function:
 */

public class TruePositiveAttributes {
	String runId;
	String reviewer;
	String transaction;
	
	public String getRunId() {
		return runId;
	}
	public void setRunId(String runId) {
		this.runId = runId;
	}
	public String getReviewer() {
		return reviewer;
	}
	public void setReviewer(String reviewer) {
		this.reviewer = reviewer;
	}
	public String getTransaction() {
		return transaction;
	}
	public void setTransaction(String transaction) {
		this.transaction = transaction;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof TruePositiveAttributes){
			TruePositiveAttributes objtp=(TruePositiveAttributes) obj;
			if((this.reviewer.equals(objtp.reviewer))&&(this.runId.equals(objtp.runId))&&(this.transaction.equals(objtp.transaction))){
				return true;
			}
			else return false;
		}
		else return false;
	}
	
	@Override
	public String toString(){
		if(reviewer!=null){
			return "RunID=\""+this.runId+"\" Reviewer=\""+this.reviewer+"\" Transaction=\""+this.transaction+"\"";
		}
		else{
			return "RunID=\""+this.runId+"\" Reviewer=\"\" Transaction=\""+this.transaction+"\"";
		}
	}
	
	@Override
	    public int hashCode() {
	        int hash = 5;
	        hash = 59 * hash + (this.runId != null ? this.runId.hashCode() : 0);
	        hash = 59 * hash + (this.reviewer != null ? this.reviewer.hashCode() : 0);
	        hash = 59 * hash + (this.transaction != null ? this.transaction.hashCode() : 0);
	        return hash;
	    }
}
