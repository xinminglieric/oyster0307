package edu.ualr.oyster.utilities;

import static org.junit.Assert.*;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FingerprintTest {
	
	private Logger logger = LoggerFactory.getLogger(getClass());

	/*
	 * Constructor has no functionality so is not tested
	 */

	@Test
	public void testGetFingerprint() {
		Fingerprint term = new Fingerprint();
		String result = term.getFingerprint("delta/Afla-CHARLIE.BrAvo");
		logger.debug("Result:[{}]", result);
		assertEquals(result,"a b c d e f h i l o r t v ");
	}

	@Test
	public void testCompareFingerprints() {
		Fingerprint term = new Fingerprint();
		String a = term.getFingerprint("delta/Afla-CHARLIE.BrAvo");
		String b = term.getFingerprint("CHARLIE.BrAvo-delta/Afla");
		logger.debug("A:[{}] B:[{}]",a,b);
		assertEquals(a,b);
	}

}
